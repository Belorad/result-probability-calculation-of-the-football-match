﻿namespace Match_result_probability_calculation
{
	internal class Factorial
	{
		//метод расчета факториала числа
		internal int GetFactorial(int k)
		{
			int factorial = 1;

			if (k > 0)
			{
				for (int i = 1; i <= k; i++)
				{
					factorial *= i;
				}
			}
			else if (k == 0)
			{
				factorial = 1;
			}
			return factorial;
		}
	}
}
