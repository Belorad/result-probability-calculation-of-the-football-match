﻿using System;

namespace Match_result_probability_calculation
{
	internal class MonteCarlo
	{
		private readonly GameDrawing gd = new GameDrawing();
		private readonly Random rnd = new Random();
		private readonly Results res = new Results();
		private readonly Limiter lim = new Limiter();
		private readonly int timeLeft = 90 - GameVars.CurrentTime;
		private readonly int scoreDifference = GameVars.EndInterval - GameVars.OwnerGoals - GameVars.GuestGoals;
		private readonly int sumGoals = GameVars.OwnerGoals + GameVars.GuestGoals;

		internal double[] PlayMonteCarlo()
		{
			int[,] drawingGameTable = gd.GetGameDrawTable(MonteCarloVar.NumberOfDraws, timeLeft, StatisticsData.averageNumberOwner, StatisticsData.averageNumberGuest, GameVars.OwnerGoals, GameVars.GuestGoals, rnd);
			double winOwner = 0;
			double winGuest = 0;
			double draw = 0;
			double uncompleteInterval = 0;
			double overInterval = 0;

			//для завершенного интервала
			for (int k = 0; k < MonteCarloVar.NumberOfDraws; k++)
			{
				int counter = GameVars.EndInterval - GameVars.OwnerGoals - GameVars.GuestGoals;

				for (int i = 0; i <= lim.GetLimiter(); i++)
				{
					for (int j = 0; j <= counter; j++)
					{
						if ((drawingGameTable[k, 1] == res.GetOwnerTable(i, j)) & (drawingGameTable[k, 2] == res.GetGuestTable(i, j)) & (drawingGameTable[k, 1] > drawingGameTable[k, 2]))
						{
							++winOwner;
						}
						else if ((drawingGameTable[k, 1] == res.GetOwnerTable(i, j)) & (drawingGameTable[k, 2] == res.GetGuestTable(i, j)) & (drawingGameTable[k, 1] < drawingGameTable[k, 2]))
						{
							++winGuest;
						}
						else if ((drawingGameTable[k, 1] == res.GetOwnerTable(i, j)) & (drawingGameTable[k, 2] == res.GetGuestTable(i, j)) & (drawingGameTable[k, 1] == drawingGameTable[k, 2]))
						{
							++draw;
						}
					}

					--counter;
				}
			}

			//для незавершенного интервала
			for (int k = 0; k < MonteCarloVar.NumberOfDraws; k++)
			{
				int counter = scoreDifference - lim.GetLimiter() - 1;

				if (GameVars.StartInterval <= sumGoals)
				{
					uncompleteInterval = 0;
				}
				else if (GameVars.StartInterval > sumGoals)
				{
					for (int i = lim.GetLimiter() + 1; i <= scoreDifference; i++)
					{
						for (int j = 0; j <= counter; j++)
						{
							if ((drawingGameTable[k, 1] == res.GetOwnerTable(i, j)) & (drawingGameTable[k, 2] == res.GetGuestTable(i, j)))
							{
								++uncompleteInterval;
							}
						}

						--counter;
					}
				}

			}

			for (int k = 0; k < MonteCarloVar.NumberOfDraws; k++)
			{
				if ((drawingGameTable[k, 1] + drawingGameTable[k, 2]) > GameVars.EndInterval)
				{
					++overInterval;
				}
			}

			double[] monteCarlo = new double[4];
			//вероятность победы хозяев
			monteCarlo[0] = winOwner / MonteCarloVar.NumberOfDraws * 100;
			//вероятность победы гостей
			monteCarlo[1] = winGuest / MonteCarloVar.NumberOfDraws * 100;
			//вероятность ничьи
			monteCarlo[2] = draw / MonteCarloVar.NumberOfDraws * 100;
			//вероятность незавершения интервала
			monteCarlo[3] = (overInterval + uncompleteInterval) / MonteCarloVar.NumberOfDraws * 100;

			return monteCarlo;
		}
	}
}
