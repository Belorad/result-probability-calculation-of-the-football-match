﻿namespace Match_result_probability_calculation
{
	internal class IntervalResult
	{
		private readonly Results res = new Results();
		private readonly Limiter lim = new Limiter();
		private int counterA = GameVars.EndInterval - GameVars.OwnerGoals - GameVars.GuestGoals;
		private int counterB = GameVars.EndInterval - GameVars.OwnerGoals - GameVars.GuestGoals;
		private int counterC = GameVars.EndInterval - GameVars.OwnerGoals - GameVars.GuestGoals;
		private double ownerWin, guestWin, draw;

		//вероятность победы хозяев на всём интервале
		internal double GetOwnerWinProbability()
		{
			for (int i = 0; i <= lim.GetLimiter(); i++)
			{
				for (int j = 0; j <= counterA; j++)
				{
					if (res.GetOwnerTable(i, j) > res.GetGuestTable(i, j))
					{
						ownerWin += res.GetProbabilitiesTable(i, j);
					}
				}

				--counterA;
			}

			return ownerWin;
		}

		//вероятность победы гостей на всём интервале
		internal double GetGuestWinProbability()
		{
			for (int i = 0; i <= lim.GetLimiter(); i++)
			{
				for (int j = 0; j <= counterB; j++)
				{
					if (res.GetOwnerTable(i, j) < res.GetGuestTable(i, j))
					{
						guestWin += res.GetProbabilitiesTable(i, j);
					}
				}

				--counterB;
			}

			return guestWin;
		}

		//вероятность ничьи на всём интервале
		internal double GetDrawProbability()
		{
			for (int i = 0; i <= lim.GetLimiter(); i++)
			{
				for (int j = 0; j <= counterC; j++)
				{
					if (res.GetOwnerTable(i, j) == res.GetGuestTable(i, j))
					{
						draw += res.GetProbabilitiesTable(i, j);
					}
				}

				--counterC;
			}

			return draw;
		}
	}
}
