﻿using System;

namespace Match_result_probability_calculation
{
	internal struct GameVars
	{
		private static int ownerGoals;
		private static int guestGoals;
		private static int startInterval;
		private static int endInterval;
		private static int currentTime;

		internal static int OwnerGoals
		{
			get
			{
				return ownerGoals;
			}
			set
			{
				ownerGoals = value;

				if (ownerGoals < 0)
				{
					throw new ArgumentOutOfRangeException("Введенное значение голов хозяев является отрицательным!");
				}
			}
		}

		internal static int GuestGoals
		{
			get
			{
				return guestGoals;
			}
			set
			{
				guestGoals = value;

				if (guestGoals < 0)
				{
					throw new ArgumentOutOfRangeException("Введенное значение голов гостей является отрицательным!");
				}
			}
		}
		internal static int StartInterval
		{
			get
			{
				return startInterval;
			}
			set
			{
				startInterval = value;

				if (startInterval < 0)
				{
					throw new ArgumentOutOfRangeException("Введенное значение начала интервала является отрицательным!");
				}
			}
		}
		internal static int EndInterval
		{
			get
			{
				return endInterval;
			}
			set
			{
				endInterval = value;

				if (endInterval < 0 ||
					endInterval - ownerGoals - guestGoals < 0 ||
					endInterval < startInterval ||
					endInterval - ownerGoals - guestGoals > 12) //для защиты факториала от переполнения
				{
					throw new ArgumentOutOfRangeException(
						"\nВведенное значение конца интервала является отрицательным" +
						"\nили заданный интервал меньше количества уже забитых голов" +
						"\nили конечный интервал меньше начального" +
						"\nили заданное верхнее значение интервала голов за гранью фантастики " +
						"\nи переполненяет факториал =) Нельзя, чтобы разница между верхним интервалом" +
						"\nи суммой забитых голов была больше 12!");
				}
			}
		}
		internal static int CurrentTime
		{
			get
			{
				return currentTime;
			}
			set
			{
				currentTime = value;

				if (currentTime < 0 || currentTime > 90)
				{
					throw new ArgumentOutOfRangeException("Введенное значение текущего времени является отрицательным или больше длительности матча!");
				}
			}
		}
	}
}
