﻿namespace Match_result_probability_calculation
{
	internal class GoalsTable
	{
		private readonly ResultTable resultTable = new ResultTable();

		//метод создания таблицы результатов команды в интервале сверх уже забитых голов плюс уже забитые
		//realScore - количество уже забитых голов хозяев или гостей на текущее время
		internal int[,] GetGoalsTable(int scoreDifference, int realScore, int fillerOne, int fillerTwo)
		{
			//создаю массив голов команды сверх уже забитых на текущее время
			int[,] goalsTable = new int[scoreDifference + 1, scoreDifference + 1];
			int counterA = scoreDifference;
			int counterB = 0;

			while (true)
			{
				if (counterA == 0)
				{
					goalsTable[counterB, 0] = realScore;
					break;
				}
				else
				{
					int[,] tableTeam = resultTable.GetResultTable(counterA + 1, counterA, fillerOne, fillerTwo);

					for (int i = 0; i <= counterA; i++)
					{
						int teamGoals = 0;

						for (int j = 0; j < counterA; j++)
						{
							teamGoals += tableTeam[i, j];
						}

						goalsTable[counterB, i] = teamGoals + realScore;
					}
				}

				counterA--;
				counterB++;
			}

			return goalsTable;
		}
	}
}
