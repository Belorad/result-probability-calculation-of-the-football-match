﻿namespace Match_result_probability_calculation
{
	internal class Limiter
	{
		private readonly int sumGoals = GameVars.OwnerGoals + GameVars.GuestGoals;

		//ограничитель в расчетах при прогоне по таблицам
		internal int GetLimiter()
		{
			int limiter = 0;

			if (GameVars.StartInterval <= sumGoals)
			{
				limiter = GameVars.EndInterval - sumGoals;
			}
			else if (GameVars.StartInterval > sumGoals)
			{
				limiter = GameVars.EndInterval - GameVars.StartInterval;
			}

			return limiter;
		}
	}
}
