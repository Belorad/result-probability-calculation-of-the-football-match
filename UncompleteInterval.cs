﻿namespace Match_result_probability_calculation
{
	internal class UncompleteInterval
	{
		private readonly FullIntervalProbabilities fullIntervalProbabilities = new FullIntervalProbabilities();
		private readonly Limiter lim = new Limiter();
		private readonly int sumGoals = GameVars.OwnerGoals + GameVars.GuestGoals;
		private readonly int scoreDifference = GameVars.EndInterval - GameVars.OwnerGoals - GameVars.GuestGoals;
		private readonly int timeLeft = 90 - GameVars.CurrentTime;

		//вероятность незавершения интервала если будут забиты голы до начала интервала
		internal double GetUncompleteIntervalProbability()
		{
			double[,] table = fullIntervalProbabilities.GetFullIntervalProbabilitiesTable(StatisticsData.averageNumberOwner, StatisticsData.averageNumberGuest, timeLeft, scoreDifference);
			double result = 0;
			int counter = scoreDifference - lim.GetLimiter() - 1;

			if (GameVars.StartInterval <= sumGoals)
			{
				result = 0;
			}
			else if (GameVars.StartInterval > sumGoals)
			{
				for (int i = lim.GetLimiter() + 1; i <= scoreDifference; i++)
				{
					for (int j = 0; j <= counter; j++)
					{
						result += table[i, j];
					}

					--counter;
				}
			}

			return result;
		}

		internal double GetFullUncompleteIntervalProbability()
		{
			double sum = 0;
			double fullResult;
			double[,] table = fullIntervalProbabilities.GetFullIntervalProbabilitiesTable(StatisticsData.averageNumberOwner, StatisticsData.averageNumberGuest, timeLeft, scoreDifference);

			for (int i = 0; i <= scoreDifference; i++)
			{
				for (int j = 0; j <= scoreDifference; j++)
				{
					sum += table[i, j];
				}
			}

			fullResult = (1 - sum + GetUncompleteIntervalProbability()) * 100;

			return fullResult;
		}
	}
}
