﻿namespace Match_result_probability_calculation
{
	internal class ResultTable
	{
		internal int[,] GetResultTable(int rows, int columns, int fillerOne, int fillerTwo) //количетсов строк таблицы (rows), количество столбцов (columns), первый наполнитель (fillerOne), второй наполнитель fillerTwo, таблица с возможными результатами свыше текущего счета до конца интервала голов
		{
			//массивы интервала голов сверх уже забитых
			int[,] resultTable = new int[rows, columns];

			//сначала заполняю массивы предварительными данными переменной fillerOne, 0 - для хозяев, 1 - для гостей
			for (int i = 0; i < rows; i++)
			{
				for (int j = 0; j < columns; j++)
				{
					resultTable.SetValue(fillerOne, i, j);
				}
			}

			//теперь заполняю нужными данными переменной fillerTwo, 1 - для хозяев, 0 - для гостей
			for (int i = 0; i < rows - 1; i++)
			{
				for (int j = columns - 1 - i; j >= 0; j--)
				{
					resultTable.SetValue(fillerTwo, i, j);
				}
			}

			return resultTable;
		}
	}
}
