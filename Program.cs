﻿using System;

namespace Match_result_probability_calculation
{
	class Program
	{
		static void Main(string[] args)
		{
			/*
			* Порядок использования классов в подготовительных расчетах:
			* ResultTable => GoalsTable
			* ResultTable => CalculationTable + Factorial => FullIntervalProbability
			*
			* Порядок основного расчета:
			* FullIntervalProbability + GameVars + StatisticsData + GoalsTable => Results
			* Results + Limiter => IntervalResult
			* FullIntervalProbability + GameVars + StatisticsData + Limiter => UncompleteInterval
			* 
			*Порядок использования классов в предварительных расчетах метода Монте-Карло:
			*RemainingTimeDrawing => GameDrawing
			*GameDrawing + Results + Limiter + MonteCarloVar => MonteCarlo
			*/

			while (true)
			{
				//На случай некорректного ввода
				try
				{
					//Ввод текущего времени
					Console.Write("Введите текущее время матча: ");
					GameVars.CurrentTime = Convert.ToInt32(Console.ReadLine());
					//Ввод количества голов хозяев поля
					Console.Write("Введите количество голов, забитых на данный момент хозяевами: ");
					//GameVars.ownerGoals = Convert.ToInt32(Console.ReadLine());
					GameVars.OwnerGoals = Convert.ToInt32(Console.ReadLine());
					//Ввод количества голов гостей поля
					Console.Write("Введите количество голов, забытых на данный момент гостями: ");
					GameVars.GuestGoals = Convert.ToInt32(Console.ReadLine());
					//Ввод начала интервала голов
					Console.Write("Введите начало интервала голов: ");
					GameVars.StartInterval = Convert.ToInt32(Console.ReadLine());
					//Ввод конца интервала голов
					Console.Write("Введите конец интервала голов: ");
					GameVars.EndInterval = Convert.ToInt32(Console.ReadLine());

				}
				catch (ArgumentOutOfRangeException outOfRange)
				{
					Console.WriteLine($"\n{outOfRange.Message}");
				}
				catch (FormatException)
				{
					Console.WriteLine("\nНЕКОРРЕКТНО ВВЕДЕННЫЕ ДАННЫЕ!");
				}

				//Просмотр полученного ввода
				Console.WriteLine("\nВерны ли введенные данные?" +
					$"\nСчет матча в настоящий момент: {GameVars.OwnerGoals} - {GameVars.GuestGoals}" +
					$"\nТекущее время матча: {GameVars.CurrentTime}" +
					$"\nЗаданный интервал {GameVars.StartInterval}...{GameVars.EndInterval}");

				Console.Write("\nВведите Y или Д, если всё верно, если нет, то N или Н: ");
				string userInput = Console.ReadLine();
				if ((String.Compare("Y", userInput, true) == 0 || String.Compare("Д", userInput, true) == 0))
				{
					if (GameVars.CurrentTime == 0 && (GameVars.OwnerGoals != 0 || GameVars.GuestGoals != 0))
					{
						Console.WriteLine("А вот и нет. На старте матча счет не может быть ненулевым!" +
							"\nВводите данные по-новой...\n");
					}
					else
					{
						break;
					}
				}
				else if (String.Compare("N", userInput, true) == 0 || String.Compare("Н", userInput, true) == 0)
				{
					Console.WriteLine("Заново вводим данные...");
				}
				else if (userInput != "Y" || userInput != "Д" || userInput != "N" || userInput != "Н")
				{
					Console.WriteLine("Введен некорректный ответ, переходим снова ко вводу данных!");
				}
			}

			//подсчет результатов завершения интервала
			Results res = new Results();
			Limiter lim = new Limiter();
			int counterA = GameVars.EndInterval - GameVars.OwnerGoals - GameVars.GuestGoals;
			int counterB = GameVars.EndInterval - GameVars.OwnerGoals - GameVars.GuestGoals;
			int counterC = GameVars.EndInterval - GameVars.OwnerGoals - GameVars.GuestGoals;
			IntervalResult intervalResult = new IntervalResult();

			Console.WriteLine($"\nВероятность победы хозяев на интервале\t  равна: {intervalResult.GetOwnerWinProbability():F2}%");

			for (int i = 0; i <= lim.GetLimiter(); i++)
			{
				for (int j = 0; j <= counterA; j++)
				{
					if (res.GetOwnerTable(i, j) > res.GetGuestTable(i, j))
					{
						Console.WriteLine($"Вероятность победы хозяев со счетом {res.GetOwnerTable(i, j)} - {res.GetGuestTable(i, j)} равна: {res.GetProbabilitiesTable(i, j):F2}%");
					}
				}

				counterA--;
			}

			Console.WriteLine($"\nВероятность победы гостей на интервале\t  равна: {intervalResult.GetGuestWinProbability():F2}%");

			for (int i = 0; i <= lim.GetLimiter(); i++)
			{
				for (int j = 0; j <= counterB; j++)
				{
					if (res.GetOwnerTable(i, j) < res.GetGuestTable(i, j))
					{
						Console.WriteLine($"Вероятность победы гостей со счетом {res.GetOwnerTable(i, j)} - {res.GetGuestTable(i, j)} равна: {res.GetProbabilitiesTable(i, j):F2}%");
					}
				}

				counterB--;
			}

			Console.WriteLine($"\nВероятность ничьи на интервале\t\t  равна: {intervalResult.GetDrawProbability():F2}%");

			for (int i = 0; i <= lim.GetLimiter(); i++)
			{
				for (int j = 0; j <= counterC; j++)
				{
					if (res.GetOwnerTable(i, j) == res.GetGuestTable(i, j))
					{
						Console.WriteLine($"Вероятность ничьи\t  со счетом {res.GetOwnerTable(i, j)} - {res.GetGuestTable(i, j)} равна: {res.GetProbabilitiesTable(i, j):F2}%");
					}
				}

				counterC--;
			}

			//Результат незавершенного интервала
			UncompleteInterval uncompleteInterval = new UncompleteInterval();

			Console.WriteLine($"\nВероятность невыполнения интервала\t  равна: {uncompleteInterval.GetFullUncompleteIntervalProbability():F2}%\n");

			//монтекарлим
			while (true)
			{
				Console.Write("\nПровести проверку расчета методом Монте-Карло?" +
					"\nВведите Y или Д, либо N или Н: ");
				string userInput = Console.ReadLine();
				if (String.Compare("Y", userInput, true) == 0 || String.Compare("Д", userInput, true) == 0)
				{
					Console.Write("Введите количество розыгрышей (1000, 10000 или что-то подобное): ");
					string userInputNumberOfDraws = Console.ReadLine();

					try
					{
						MonteCarloVar.NumberOfDraws = Convert.ToInt32(userInputNumberOfDraws);
					}
					catch (ArgumentOutOfRangeException outOfRange)
					{
						Console.WriteLine($"\n{outOfRange.Message}");
					}
					catch (FormatException)
					{
						Console.WriteLine("\nНЕКОРРЕКТНО ВВЕДЕННЫЕ ДАННЫЕ!");
					}

					if (MonteCarloVar.NumberOfDraws > 0)
					{
						MonteCarlo monteCarlo = new MonteCarlo();
						double[] drawResults = monteCarlo.PlayMonteCarlo();
						Console.WriteLine($"\nВероятность победы хозяев равна:\t\t{drawResults[0]:F2}%");
						Console.WriteLine($"Вероятность победы гостей равна:\t\t{drawResults[1]:F2}%");
						if (drawResults[2] != 0)
						{
							Console.WriteLine($"Вероятность ничьи равна:\t\t\t{drawResults[2]:F2}%");
						}
						Console.WriteLine($"Вероятность невыполнения интервала равна:\t{drawResults[3]:F2}%");
						break;
					}
				}
				else if (String.Compare("N", userInput, true) == 0 || String.Compare("Н", userInput, true) == 0)
				{
					break;
				}
				else if (userInput != "Y" || userInput != "Д" || userInput != "N" || userInput != "Н")
				{
					Console.WriteLine("Введен некорректный ответ, переходим снова ко вводу данных!");
				}
			}

			Console.WriteLine("Расчеты выполнены, нажмите любую клавишу...");
			Console.ReadKey();
		}
	}
}
