﻿using System;

namespace Match_result_probability_calculation
{
	internal class GameDrawing
	{
		private readonly RemainingTimeDrawing remainingTimeDrawing = new RemainingTimeDrawing();
		//Метод переигрывания оставшегося времени матча (10000 раз)
		internal int[,] GetGameDrawTable(int numberOfDraws, int timeLeft, double averageNumberOwner, double averageNumberGuest, int ownerGoals, int guestGoals, Random rnd)
		{
			int drawingNumber = 0;
			int[,] table = new int[numberOfDraws, 3];

			for (int i = 0; i < numberOfDraws; i++)
			{
				//номер розыгрыша
				table[i, 0] = ++drawingNumber;
				//счет хозяев
				table[i, 1] = remainingTimeDrawing.GetScore(timeLeft, averageNumberOwner, rnd) + ownerGoals;
				//счет гостей
				table[i, 2] = remainingTimeDrawing.GetScore(timeLeft, averageNumberGuest, rnd) + guestGoals;
			}

			return table;
		}
	}
}
