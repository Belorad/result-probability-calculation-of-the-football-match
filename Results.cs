﻿namespace Match_result_probability_calculation
{
	internal class Results
	{
		private readonly FullIntervalProbabilities fullIntervalProbabilities = new FullIntervalProbabilities();
		private readonly GoalsTable goalsTable = new GoalsTable();
		private readonly int scoreDifference = GameVars.EndInterval - GameVars.OwnerGoals - GameVars.GuestGoals;
		private readonly int timeLeft = 90 - GameVars.CurrentTime;

		//таблица с вероятностями исходов матчей от нуля голов от текущего счета до конечного интервала
		internal double GetProbabilitiesTable(int i, int j)
		{
			double[,] table = fullIntervalProbabilities.GetFullIntervalProbabilitiesTable(StatisticsData.averageNumberOwner, StatisticsData.averageNumberGuest, timeLeft, scoreDifference);

			return table[i, j] * 100;
		}

		//таблица с соответсвующим вероятностям голами хозяев с учетом уже забитых голов
		internal int GetOwnerTable(int i, int j)
		{
			int[,] table = goalsTable.GetGoalsTable(scoreDifference, GameVars.OwnerGoals, fillerOne: 0, fillerTwo: 1);

			return table[i, j];
		}

		//таблица с соответсвующим вороятностям голами гостей с учетом уже забитых голов
		internal int GetGuestTable(int i, int j)
		{
			int[,] table = goalsTable.GetGoalsTable(scoreDifference, GameVars.GuestGoals, fillerOne: 1, fillerTwo: 0);

			return table[i, j];
		}

	}
}
