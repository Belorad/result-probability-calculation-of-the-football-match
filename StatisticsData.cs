﻿using System.Collections.Generic;

namespace Match_result_probability_calculation
{
	internal static class StatisticsData
	{
		internal static readonly double averageNumberOwner, averageNumberGuest;
		static StatisticsData()
		{
			Dictionary<string, double> statistics = new Dictionary<string, double>(); //словарь со статистикой матчей
			#region //внесенные значения в словарь
			statistics.Add("0-0", 4.33);
			statistics.Add("0-1", 6.05);
			statistics.Add("0-2", 4.24);
			statistics.Add("0-3", 1.98);
			statistics.Add("0-4", 0.70);
			statistics.Add("0-5", 0.19);
			statistics.Add("1-0", 7.57);
			statistics.Add("1-1", 10.59);
			statistics.Add("1-2", 7.41);
			statistics.Add("1-3", 3.46);
			statistics.Add("1-4", 1.21);
			statistics.Add("1-5", 0.34);
			statistics.Add("2-0", 6.62);
			statistics.Add("2-1", 9.27);
			statistics.Add("2-2", 6.48);
			statistics.Add("2-3", 3.03);
			statistics.Add("2-4", 1.06);
			statistics.Add("2-5", 0.29);
			statistics.Add("3-0", 3.86);
			statistics.Add("3-1", 5.41);
			statistics.Add("3-2", 3.78);
			statistics.Add("3-3", 1.76);
			statistics.Add("3-4", 0.62);
			statistics.Add("3-5", 0.17);
			statistics.Add("4-0", 1.68);
			statistics.Add("4-1", 2.36);
			statistics.Add("4-2", 1.65);
			statistics.Add("4-3", 0.78);
			statistics.Add("4-4", 0.27);
			statistics.Add("5-0", 0.59);
			statistics.Add("5-1", 0.83);
			statistics.Add("5-2", 0.57);
			statistics.Add("5-3", 0.27);
			statistics.Add("6-0", 0.17);
			statistics.Add("6-1", 0.24);
			statistics.Add("6-2", 0.17);
			#endregion

			#region//для расчета среднего числа голов хозяев
			double owner0 = statistics["0-0"] + statistics["0-1"] + statistics["0-2"] + statistics["0-3"] + statistics["0-4"] + statistics["0-5"];
			double owner1 = statistics["1-0"] + statistics["1-1"] + statistics["1-2"] + statistics["1-3"] + statistics["1-4"] + statistics["1-5"];
			double owner2 = statistics["2-0"] + statistics["2-1"] + statistics["2-2"] + statistics["2-3"] + statistics["2-4"] + statistics["2-5"];
			double owner3 = statistics["3-0"] + statistics["3-1"] + statistics["3-2"] + statistics["3-3"] + statistics["3-4"] + statistics["3-5"];
			double owner4 = statistics["4-0"] + statistics["4-1"] + statistics["4-2"] + statistics["4-3"] + statistics["4-4"];
			double owner5 = statistics["5-0"] + statistics["5-1"] + statistics["5-2"] + statistics["5-3"];
			double owner6 = statistics["6-0"] + statistics["6-1"] + statistics["6-2"];
			#endregion

			#region//для расчета среднего числа голов гостей
			double guest0 = statistics["0-0"] + statistics["1-0"] + statistics["2-0"] + statistics["3-0"] + statistics["4-0"] + statistics["5-0"] + statistics["6-0"];
			double guest1 = statistics["0-1"] + statistics["1-1"] + statistics["2-1"] + statistics["3-1"] + statistics["4-1"] + statistics["5-1"] + statistics["6-1"];
			double guest2 = statistics["0-2"] + statistics["1-2"] + statistics["2-2"] + statistics["3-2"] + statistics["4-2"] + statistics["5-2"] + statistics["6-2"];
			double guest3 = statistics["0-3"] + statistics["1-3"] + statistics["2-3"] + statistics["3-3"] + statistics["4-3"] + statistics["5-3"];
			double guest4 = statistics["0-4"] + statistics["1-4"] + statistics["2-4"] + statistics["3-4"] + statistics["4-4"];
			double guest5 = statistics["0-5"] + statistics["1-5"] + statistics["2-5"] + statistics["3-5"];
			#endregion

			//расчет среднего числа голов, забиваемых хозяевами и гостями за матч, то есть математического ожидания числа голов за матч для хозяев и гостей по отдельности
			averageNumberOwner = (owner0 * 0 + owner1 * 1 + owner2 * 2 + owner3 * 3 + owner4 * 4 + owner5 * 5 + owner6 * 6) / 100;
			averageNumberGuest = (guest0 * 0 + guest1 * 1 + guest2 * 2 + guest3 * 3 + guest4 * 4 + guest5 * 5) / 100;
		}
	}
}
