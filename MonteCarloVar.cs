﻿using System;

namespace Match_result_probability_calculation
{
	internal struct MonteCarloVar
	{
		private static int numberOfDraws;
		internal static int NumberOfDraws
		{
			get
			{
				return numberOfDraws;
			}
			set
			{
				numberOfDraws = value;

				if (numberOfDraws < 1)
				{
					throw new ArgumentOutOfRangeException("Введенное значение является отрицательным или равно нулю!");
				}
			}
		}
	}
}
