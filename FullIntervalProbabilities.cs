﻿using System;

namespace Match_result_probability_calculation
{
	internal class FullIntervalProbabilities
	{
		private readonly CalculationTable calculationTable = new CalculationTable();
		private readonly Factorial factorial = new Factorial();

		//метод расчета вероятностей полного интервала - сверх уже забитых голов до верхнего интервала включительно
		internal double[,] GetFullIntervalProbabilitiesTable(double averageNumberOwner, double averageNumberGuest, int timeLeft, int scoreDifference)
		{
			int[,] calculationForOwner = calculationTable.GetCalculationTable(scoreDifference, fillerOne: 0, fillerTwo: 1);
			int[,] calculationForGuest = calculationTable.GetCalculationTable(scoreDifference, fillerOne: 1, fillerTwo: 0);
			double e = Math.E;

			//создаю массив содержащий вероятности, в первой строке верхний интервал, во второй - на гол меньше и так до нуля забиваемых голов свыше реального счета
			double[,] probabilitiesTable = new double[scoreDifference + 1, scoreDifference + 1];
			int counter = scoreDifference;

			for (int i = 0; i <= scoreDifference; i++)
			{
				for (int j = 0; j <= counter; j++)
				{
					int kOwner = calculationForOwner[i, j];
					int kGuest = calculationForGuest[i, j];
					double ownerProbability = Math.Pow(averageNumberOwner / 90 * timeLeft, kOwner) * Math.Pow(e, -averageNumberOwner / 90 * timeLeft) / factorial.GetFactorial(kOwner);
					double guestProbability = Math.Pow(averageNumberGuest / 90 * timeLeft, kGuest) * Math.Pow(e, -averageNumberGuest / 90 * timeLeft) / factorial.GetFactorial(kGuest);
					//записываю вероятность в строку массива
					probabilitiesTable[i, j] = ownerProbability * guestProbability;
				}

				counter--;
			}

			return probabilitiesTable;
		}
	}
}
