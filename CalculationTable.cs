﻿namespace Match_result_probability_calculation
{
	internal class CalculationTable
	{
		private readonly ResultTable resultTable = new ResultTable();

		//метод создания таблицы результатов команды в интервале сверх уже забитых голов для дальнейшего расчета вероятностей
		internal int[,] GetCalculationTable(int scoreDifference, int fillerOne, int fillerTwo)
		{
			//создаю массив голов команды сверх уже забитых на текущее время
			int[,] calculationTable = new int[scoreDifference + 1, scoreDifference + 1];
			int counterA = scoreDifference;
			int counterB = 0;

			while (true)
			{
				if (counterA == 0)
				{
					calculationTable[counterB, 0] = 0;
					break;
				}
				else
				{
					int[,] tableTeam = resultTable.GetResultTable(counterA + 1, counterA, fillerOne, fillerTwo);

					for (int i = 0; i < counterA + 1; i++)
					{
						int teamGoals = 0;

						for (int j = 0; j < counterA; j++)
						{
							teamGoals += tableTeam[i, j];
						}

						calculationTable[counterB, i] = teamGoals;
					}
				}

				counterA--;
				counterB++;
			}

			return calculationTable;
		}
	}
}
