﻿using System;

namespace Match_result_probability_calculation
{
	internal class RemainingTimeDrawing
	{
		//Метод расчета розыгрыша каждой минуты до конца матча за оставшееся время
		internal int GetScore(int timeLeft, double averageNumber, Random rnd)
		{
			int score = 0;

			for (int i = 0; i < timeLeft; i++)
			{
				double randomNumber = rnd.NextDouble();
				if (randomNumber < averageNumber / 90)
				{
					score++;
				}
			}

			return score;
		}
	}
}
